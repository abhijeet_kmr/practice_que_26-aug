const data = {
  person_info_1: {
    profile: {
      fullName: "Javier Hernandez",
      nationality: {
        country: "Mexico",
      },
      tel: 904902394,
    },
    current_address: {
      current_city: {
        value: "Bangalore",
        zip: "399993",
      },
    },
  },

  person_info_2: {
    profile: {
      fullName: "Emily Spade",
      nationality: {
        country: "Norway",
      },
      tel: 309320239,
    },
    current_address: {
      current_city: {
        value: "Oslo",
        zip: "239292",
      },
    },
  },
  person_info_3: {
    profile: {
      fullName: "John Cigan",
      nationality: {
        country: "Turkey",
      },
      tel: 932483988,
    },
    current_address: {
      current_city: {
        value: "Istanbul",
        zip: "932099",
      },
    },
  },
  person_info_4: {
    profile: {
      fullName: "Marsh Hobbs",
      nationality: {
        country: "USA",
      },
      tel: 32043988,
    },
    current_address: {
      current_city: {
        value: "Istanbul",
        zip: "932099",
      },
    },
  },
};

//  Q1. Use reduce to generate following data..

//  const result = [{
//     profile: {
//         fullName: "Javier Hernandez",
//         nationality: {
//             country: "Mexico",
//         },
//         tel: 904902394
//     },
//     current_address: {
//         current_city: {
//             value: "Bangalore",
//             zip: '399993'
//         }
//     }
// },
// {
//     profile: {
//         fullName: "Emily Spade",
//         nationality: {
//             country: "Norway",
//         },
//         tel: 309320239
//     },
//     current_address: {
//         current_city: {
//             value: "Oslo",
//             zip: '239292'
//         }
//     }
// },
//     ... // Rest other objects
// ]

// Ans1..
const result1 = Object.keys(data).reduce((acc, curr) => {
  //   console.log(data[curr]);
  let toPush = data[curr];
  acc.push(toPush);
  return acc;
}, []);
console.log(result1);
console.log(...result1);

// Q2.Get data in following format using reduce

// [[fullName, country], [fullName, country] ... for all objects]

// Ans2..
const result2 = Object.keys(data).reduce((acc, curr) => {
  let fullName = data[curr].profile.fullName;
  let countries = data[curr].profile.nationality.country;
  let arr = [fullName, countries];
  acc.push(arr);
  return acc;
}, []);
console.log(result2);

// Q3.Restructure data and bring tel 'key' outside person .

// const result = {
//     person_info_1: {
//         profile: {
//             fullName: "Javier Hernandez",
//             nationality: {
//                 country: "Mexico",
//             },
//         },
//         tel: 904902394,          // Moved outside
//         current_address: {
//             current_city: {
//                 value: "Bangalore",
//                 zip: '399993'
//             }
//         }
//     },
//     ... rest other objects

// }

// Ans3..
const result3 = Object.keys(data).reduce((acc, curr) => {
  const { profile, current_address } = data[curr];
  const { tel, ...rest } = profile;
  acc[curr] = {
    profile: rest,
    tel: tel,
    current_address: current_address,
  };

  return acc;
}, {});
console.log(result3);
